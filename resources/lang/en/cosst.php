<?php

return [

    /*
    |--------------------------------------------------------------------------
    | COSST Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the core COSST software to
    | allow for localisation.
    |
    */

    'welcome_title' => 'Welcome',
    'see_more' => 'See More',
    'dashboard' => 'Dashboard',
    'knowledgebase' => 'Knowledgebase',
    'announcements' => 'Announcements',
    'service_status' => 'Service Status',
    'register' => 'Register',
    'login' => 'Login',
    'welcome' => 'Welcome',
    'welcometo' => 'Welcome to',
    'viewprofilelink' => 'View Profile'
];
