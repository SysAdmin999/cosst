COSST | Completely Open-Source Support Ticketing
=====

COSST is an open-source Support Ticketing system based entirely on the Laravel Framework.

It's is a new project and contributions are welcomed.

If you'd like to be actively involved, please get in touch via the email below.

github@cosst.co.uk
